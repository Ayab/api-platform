<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\DrunkRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=DrunkRepository::class)
 */
class Drunk
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="drunk", orphanRemoval=true)
     */
    private $userId;

    /**
     * @ORM\OneToMany(targetEntity=Drink::class, mappedBy="drunk", orphanRemoval=true)
     */
    private $drinkId;
    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", options={"default":"CURRENT_TIMESTAMP"})
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", options={"default":"CURRENT_TIMESTAMP"})
     */
    private $updated;

    public function __construct()
    {
        $this->userId = new ArrayCollection();
        $this->drinkId = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUserId(): Collection
    {
        return $this->userId;
    }

    public function addUserId(User $userId): self
    {
        if (!$this->userId->contains($userId)) {
            $this->userId[] = $userId;
            $userId->setDrunk($this);
        }

        return $this;
    }

    public function removeUserId(User $userId): self
    {
        if ($this->userId->contains($userId)) {
            $this->userId->removeElement($userId);
            // set the owning side to null (unless already changed)
            if ($userId->getDrunk() === $this) {
                $userId->setDrunk(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Drink[]
     */
    public function getDrinkId(): Collection
    {
        return $this->drinkId;
    }

    public function addDrinkId(Drink $drinkId): self
    {
        if (!$this->drinkId->contains($drinkId)) {
            $this->drinkId[] = $drinkId;
            $drinkId->setDrunk($this);
        }

        return $this;
    }

    public function removeDrinkId(Drink $drinkId): self
    {
        if ($this->drinkId->contains($drinkId)) {
            $this->drinkId->removeElement($drinkId);
            // set the owning side to null (unless already changed)
            if ($drinkId->getDrunk() === $this) {
                $drinkId->setDrunk(null);
            }
        }

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created): void
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated): void
    {
        $this->updated = $updated;
    }

}
